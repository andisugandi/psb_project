from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic
from formtools.wizard.views import SessionWizardView
from django.contrib.auth.models import User
from django.views.generic.base import View
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login
from wkhtmltopdf.views import PDFTemplateResponse

from .models import Sekolah, CalonSiswa
from psb_project import settings


def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('waktu_ujian'))
    else:
        return HttpResponseRedirect(reverse('login'))


class CalonSiswaWizard(SessionWizardView):
    def done(self, form_list, **kwargs):
        registration_data = [form.cleaned_data for form in form_list]

        # Insert data user
        new_user = User.objects.create_user(username=registration_data[0]['username'],
                                            email=registration_data[0]['email'],
                                            password=registration_data[0]['password'])

        # Insert data calon siswa
        new_profile = CalonSiswa()
        new_profile.nama_lengkap = registration_data[1]['nama_lengkap']
        new_profile.agama = registration_data[1]['agama']
        new_profile.nama_ayah = registration_data[1]['nama_ayah']
        new_profile.nama_ibu = registration_data[1]['nama_ibu']
        new_profile.alamat_orang_tua = registration_data[1]['alamat_orang_tua']
        new_profile.alamat_calon_siswa = registration_data[1]['alamat_calon_siswa']
        new_profile.nilai_un = registration_data[1]['nilai_un']
        new_profile.nilai_akhir = new_profile.nilai_un
        new_profile.user = new_user
        new_profile.sekolah = registration_data[1]['sekolah']
        new_profile.save()

        # Mengirim email informasi pendaftaran
        subject = 'INFO REGISTRASI PENERIMAAN SISWA BARU SMK XXX BANDUNG'
        message = '''Hai, terimakasih sudah mendaftarkan diri ke SMK XXX Bandung.
Berikut adalah informasi mengenai pendaftaran anda:
Username: {0}
Email: {1}

Dimohon untuk datang 30 menit lebih awal sebelum waktu ujian dimulai.'''.format(new_user.username, new_user.email,
                                                                                registration_data[0]['password'])
        from_email = settings.EMAIL_HOST_USER
        to_email = [new_user.email]
        send_mail(subject, message, from_email, to_email, fail_silently=False)

        user = authenticate(username=new_user.username,
                            password=registration_data[0]['password'])
        login(self.request, user)

        return HttpResponseRedirect('/')


class DataCalonSiswa(View):
    template = 'app/data_calon_siswa.html'

    def get(self, request):
        if request.user.is_authenticated():
            calon_siswa = CalonSiswa.objects.filter(user=request.user)
            context = {'calon_siswa': calon_siswa[0]}

            return render(request, self.template, context)
        else:
            return HttpResponseRedirect(reverse('login'))


class WaktuUjianView(View):
    template = 'app/waktu_ujian.html'

    def get(self, request):
        if request.user.is_authenticated():
            data_ujian = {'sekolah': settings.NAMA_SEKOLAH,
                          'alamat_sekolah': settings.ALAMAT_SEKOLAH,
                          'kepsek': settings.KEPSEK,
                          'tgl_peresmian': settings.TGL_PERESMIAN,
                          'tgl_ujian': settings.TGL_UJIAN}
            calon_siswa = CalonSiswa.objects.filter(user=request.user)
            context = {'calon_siswa': calon_siswa[0],
                       'data_ujian': data_ujian}

            return render(request, self.template, context)
        else:
            return HttpResponseRedirect(reverse('login'))


class CetakKartuUjianView(View):
    template = 'app/kartu_ujian.html'

    def get(self, request):
        filename = 'kartu_ujian.pdf'

        if request.user.is_authenticated():
            data_ujian = {'sekolah': settings.NAMA_SEKOLAH,
                          'kepsek': settings.KEPSEK,
                          'tgl_peresmian': settings.TGL_PERESMIAN,
                          'tgl_ujian': settings.TGL_UJIAN}
            calon_siswa = CalonSiswa.objects.filter(user=request.user)
            context = {'calon_siswa': calon_siswa[0],
                       'data_ujian': data_ujian,
                       'STATIC_ROOT': settings.STATIC_ROOT}

            response = PDFTemplateResponse(request=request,
                                           template=self.template,
                                           filename=filename,
                                           context=context,
                                           show_content_in_browser=False)

            return response
        else:
            return HttpResponseRedirect(reverse('login'))
