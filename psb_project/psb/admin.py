from django.contrib import admin
from .models import CalonSiswa, Sekolah
from import_export import resources
from import_export.admin import ImportExportModelAdmin


class CalonSiswaResource(resources.ModelResource):
    class Meta:
        model = CalonSiswa
        skip_unchanged = True
        fields = (
            'id',
            'nama_lengkap',
            'sekolah__nama_sekolah',
            'nilai_un',
            'nilai_ujian',
            'rekomendasi',
            'nilai_akhir',
        )
        export_order = (
            'id',
            'nama_lengkap',
            'sekolah__nama_sekolah',
            'nilai_un',
            'nilai_ujian',
            'rekomendasi',
            'nilai_akhir',
        )


class CalonSiswaAdmin(ImportExportModelAdmin):
    list_display = (
        'user',
        'nama_lengkap',
        'sekolah',
        'nilai_un',
        'nilai_ujian',
        'rekomendasi',
        'nilai_akhir',
    )
    list_per_page = 40
    search_fields = (
        'user__username',
        'nama_lengkap',
        'sekolah__nama_sekolah',
    )
    readonly_fields = ('user',)
    resource_class = CalonSiswaResource

    class Meta:
        model = CalonSiswa


class SekolahAdmin(admin.ModelAdmin):
    list_display = (
        'nama_sekolah',
        'alamat_sekolah',
    )
    list_per_page = 40
    search_fields = (
        'nama_sekolah',
        'alamat_sekolah',
    )

    class Meta:
        model = Sekolah


admin.site.register(CalonSiswa, CalonSiswaAdmin)
admin.site.register(Sekolah, SekolahAdmin)
