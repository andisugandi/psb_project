from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.db import models

class Sekolah(models.Model):
    nama_sekolah = models.CharField('Nama Sekolah', max_length=254,
                                    validators=[RegexValidator(regex='^[A-Za-z0-9 ]*$')])
    alamat_sekolah = models.CharField('Alamat Sekolah', max_length=254,
                                      validators=[RegexValidator(regex='^[A-Za-z0-9 ]*$')])

    class Meta:
        verbose_name = 'Sekolah'
        verbose_name_plural = 'Data Sekolah'

    def __unicode__(self):
        return self.nama_sekolah


class CalonSiswa(models.Model):
    AGAMA = (
        ('Islam', 'Islam'),
        ('Kristen', 'Kristen'),
        ('Katolik', 'Katolik'),
        ('Hindu', 'Hindu'),
        ('Budha', 'Budha'),
        ('Kong Hu Cu', 'Kong Hu Cu'),
    )

    REKOMENDASI = (
        (0, 0),
        (1, 1),
    )

    user = models.ForeignKey(User)
    sekolah = models.ForeignKey(Sekolah)
    nama_lengkap = models.CharField('Nama Lengkap', max_length=64, validators=[RegexValidator(regex='^[A-Za-z ]*$')])
    agama = models.CharField(choices=AGAMA, max_length=10)
    nama_ayah = models.CharField('Nama Ayah', max_length=64, validators=[RegexValidator(regex='^[A-Za-z ]*$')])
    nama_ibu = models.CharField('Nama Ibu', max_length=64, validators=[RegexValidator(regex='^[A-Za-z ]*$')])
    alamat_orang_tua = models.CharField('Alamat Orang Tua', max_length=128,
                                        validators=[RegexValidator(regex='^[A-Za-z0-9 ]*$')])
    alamat_calon_siswa = models.CharField('Alamat Calon Siswa', max_length=128,
                                          validators=[RegexValidator(regex='^[A-Za-z0-9 ]*$')])
    nilai_un = models.DecimalField('Nilai UN', max_digits=5, decimal_places=2, default=0.0,
                                   validators=[MinValueValidator(0), MaxValueValidator(100)])
    nilai_ujian = models.DecimalField('Nilai Ujian', max_digits=5, decimal_places=2, default=0.0,
                                      validators=[MinValueValidator(0), MaxValueValidator(100)])
    nilai_akhir = models.DecimalField('Nilai Akhir', max_digits=5, decimal_places=2, default=0.0,
                                      validators=[MinValueValidator(0), MaxValueValidator(100)])
    rekomendasi = models.SmallIntegerField(choices=REKOMENDASI, default=0)

    class Meta:
        verbose_name = 'Calon Siswa'
        verbose_name_plural = 'Data Calon Siswa'
        ordering = ('user',)

    def __unicode__(self):
        return self.nama_lengkap
