from django import forms

from .models import CalonSiswa


class ProfileCalonSiswa(forms.ModelForm):
    class Meta:
        model = CalonSiswa
        fields = (
            'nama_lengkap',
            'sekolah',
            'agama',
            'nama_ayah',
            'nama_ibu',
            'alamat_orang_tua',
            'alamat_calon_siswa',
            'nilai_un',
        )


class UserSiswa(forms.Form):
    username = forms.CharField(min_length=3, max_length=30)
    email = forms.EmailField()
    password = forms.CharField(min_length=8, widget=forms.PasswordInput)
