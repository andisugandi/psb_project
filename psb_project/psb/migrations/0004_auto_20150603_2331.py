# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('psb', '0003_auto_20150603_2306'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sekolah',
            name='alamat_sekolah',
            field=models.CharField(max_length=254, verbose_name=b'Alamat Sekolah', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z0-9 ]*$')]),
        ),
    ]
