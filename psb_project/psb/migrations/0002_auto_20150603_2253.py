# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('psb', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calonsiswa',
            name='alamat_calon_siswa',
            field=models.CharField(max_length=128, verbose_name=b'Alamat Calon Siswa'),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='alamat_orang_tua',
            field=models.CharField(max_length=128, verbose_name=b'Alamat Orang Tua'),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nama_ayah',
            field=models.CharField(max_length=64, verbose_name=b'Nama Ayah'),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nama_ibu',
            field=models.CharField(max_length=64, verbose_name=b'Nama Ibu'),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nama_lengkap',
            field=models.CharField(max_length=64, verbose_name=b'Nama Lengkap'),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nilai_akhir',
            field=models.DecimalField(default=0.0, verbose_name=b'Nilai Akhir', max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nilai_ujian',
            field=models.DecimalField(default=0.0, verbose_name=b'Nilai Ujian', max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nilai_un',
            field=models.DecimalField(default=0.0, verbose_name=b'Nilai UN', max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
    ]
