# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CalonSiswa',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('nama_lengkap', models.CharField(blank=True, verbose_name='Nama Lengkap', max_length=64)),
                ('agama', models.CharField(choices=[('Islam', 'Islam'), ('Kristen', 'Kristen'), ('Katolik', 'Katolik'), ('Hindu', 'Hindu'), ('Budha', 'Budha'), ('Kong Hu Cu', 'Kong Hu Cu')], max_length=10)),
                ('nama_ayah', models.CharField(blank=True, verbose_name='Nama Ayah', max_length=64)),
                ('nama_ibu', models.CharField(blank=True, verbose_name='Nama Ibu', max_length=64)),
                ('alamat_orang_tua', models.CharField(blank=True, verbose_name='Alamat Orang Tua', max_length=128)),
                ('alamat_calon_siswa', models.CharField(blank=True, verbose_name='Alamat Calon Siswa', max_length=128)),
                ('nilai_un', models.DecimalField(default=0.0, decimal_places=2, verbose_name='Nilai UN', max_digits=5)),
                ('nilai_ujian', models.DecimalField(default=0.0, decimal_places=2, verbose_name='Nilai Ujian', max_digits=5)),
                ('nilai_akhir', models.DecimalField(default=0.0, decimal_places=2, verbose_name='Nilai Akhir', max_digits=5)),
                ('rekomendasi', models.SmallIntegerField(default=0, choices=[(0, 0), (1, 1)])),
            ],
            options={
                'ordering': ('user',),
                'verbose_name_plural': 'Data Calon Siswa',
                'verbose_name': 'Calon Siswa',
            },
        ),
        migrations.CreateModel(
            name='Sekolah',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('nama_sekolah', models.CharField(verbose_name='Nama Sekolah', max_length=254)),
                ('alamat_sekolah', models.CharField(verbose_name='Alamat Sekolah', max_length=254)),
            ],
            options={
                'verbose_name_plural': 'Data Sekolah',
                'verbose_name': 'Sekolah',
            },
        ),
        migrations.AddField(
            model_name='calonsiswa',
            name='sekolah',
            field=models.ForeignKey(to='psb.Sekolah'),
        ),
        migrations.AddField(
            model_name='calonsiswa',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
