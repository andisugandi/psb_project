# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('psb', '0002_auto_20150603_2253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calonsiswa',
            name='alamat_calon_siswa',
            field=models.CharField(max_length=128, verbose_name=b'Alamat Calon Siswa', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z0-9 ]*$')]),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='alamat_orang_tua',
            field=models.CharField(max_length=128, verbose_name=b'Alamat Orang Tua', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z0-9 ]*$')]),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nama_ayah',
            field=models.CharField(max_length=64, verbose_name=b'Nama Ayah', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z ]*$')]),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nama_ibu',
            field=models.CharField(max_length=64, verbose_name=b'Nama Ibu', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z ]*$')]),
        ),
        migrations.AlterField(
            model_name='calonsiswa',
            name='nama_lengkap',
            field=models.CharField(max_length=64, verbose_name=b'Nama Lengkap', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z ]*$')]),
        ),
        migrations.AlterField(
            model_name='sekolah',
            name='alamat_sekolah',
            field=models.CharField(max_length=254, verbose_name=b'Alamat Sekolah', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z ]*$')]),
        ),
        migrations.AlterField(
            model_name='sekolah',
            name='nama_sekolah',
            field=models.CharField(max_length=254, verbose_name=b'Nama Sekolah', validators=[django.core.validators.RegexValidator(regex=b'^[A-Za-z ]*$')]),
        ),
    ]
