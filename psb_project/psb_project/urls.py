from django.conf.urls import include, url
from django.contrib import admin
from django.core.urlresolvers import get_callable
from django.contrib.auth import views as auth_views

from psb.general_function import anonymous_required
from psb import views
from psb.forms import UserSiswa, ProfileCalonSiswa
from psb.views import CalonSiswaWizard, CetakKartuUjianView

urlpatterns = [
    url(r'^login/$', anonymous_required(get_callable('django.contrib.auth.views.login')), name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/login/'}, name='logout'),

    url(r'^password_change/$', auth_views.password_change, name='password_change'),
    url(r'^password_change/done/$', auth_views.password_change, name='password_change_done'),

    url(r'^password_reset/$', auth_views.password_reset, {'template_name': 'registration/password_reset.html'},
        name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done,
        {'template_name': 'registration/password_reset_sent.html'},
        name='password_reset_done'),

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.home, name='home'),
    url(r'^register/', anonymous_required(CalonSiswaWizard.as_view([UserSiswa, ProfileCalonSiswa])), name='register'),
    url(r'^cetak_kartu_ujian$', CetakKartuUjianView.as_view(), name='cetak_kartu_ujian'),
    url(r'^calon_siswa/$', views.DataCalonSiswa.as_view(), name='data_calon_siswa'),
    url(r'^waktu_ujian/$', views.WaktuUjianView.as_view(), name='waktu_ujian'),
]
